; This gcode is formulated to work with dual tool head test stands running TAZ 6 Dual v2 firmware version 1.1.9.9
; Use of this gcode outside of an identical setup is likely to provide unexpected results
T0
M104 S205
T1
M104 S205
M117 FANS 100 PERCENT
M106 P0 S255		     ; set cooling fan 0 to 100%
M106 P1 S255		     ; set cooling fan 1 to 100%

G4 S10			     ; dwell 10 seconds

M117 FANS OFF
M107
M106 P1 S0


G4 S5			     ; dwell 5 seconds


M117 FANS 40 PERCENT
M106 P0 S102		     ; set cooling fan 0 to 40%
M106 P1 S102		     ; set cooling fan 1 to 40%

G4 S10			     ; dwell 10 seconds

M117 FANS OFF
M107
M106 P1 S0


G4 S5			     ; dwell 5 seconds

M999                 ; clear errors
M400                 ; clear buffer
G21                  ; set units to millimeters
M82                  ; use absolute distances for extrusion
T0                   ; tool 0
G92 E0               ; Set cords to zero
T1                   ; tool 1
G92 E0               ; Set cords to zero
M92 T0 E420          ; set T0 esteps to base 420
M92 T1 E420          ; set T1 esteps to base 420
M500                 ; save into memory

T1
M104 S205

T0                   ; select tool 0
M109 S205            ; set extruder nozzle to 205C and wait
M117 Extruding
G1 E100 F90          ; move extruder 1 100mm
G4 S5
M117 Retracting
G92 E0
G1 E-45 F250         ; retract filament 45mm
G4 S1
M300 S5
M117 PULL FILAMENT
G4 S5
G92 E0
G1 E-45 F500
G4 S1                ; wait

T1                   ; select tool 1
M109 S205            ; set extruder nozzle to 205C and wait
M117 Extruding
G1 E100 F90          ; move extruder 1 100mm
G4 S5
M117 Retracting
G92 E0
G1 E-45 F250         ; retract filament 45mm
G4 S1
M300 S5
M117 PULL FILAMENT
G4 S5
G92 E0
G1 E-45 F500
G4 S1                ; wait

M106 S255            ; turn E0 Fan on 100 percent
M106 P1 S255         ; turn E1 Fan on 100 percent

T0
M104 S60
T1
M109 R60
M106 P0 S0           ; fan off
M106 P1 S0           ; fan off
M84                  ; idle motors
M18                  ; turn off motors
M300 S5              ; beep for end
M117 Test Complete

